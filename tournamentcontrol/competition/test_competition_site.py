from datetime import date

from django.core.urlresolvers import reverse_lazy
from test_plus import TestCase

from tournamentcontrol.competition.models import Match


class GoodViewTests(TestCase):

    fixtures = [
        'user.json',
        'competition.json',
        'draw_format.json',
        'club.json',
        'person.json',
        'season.json',
        'place.json',
        'division.json',
        'stage.json',
        'team.json',
        'match.json',
    ]

    def test_index(self):
        self.assertGoodView('competition:index')

    def test_competition(self):
        self.assertGoodView('competition:competition',
                            'sydney-university')

    def test_season(self):
        self.assertGoodView('competition:season',
                            'sydney-university',
                            'summer-2012')

    def test_season_calendar(self):
        # self.assertGoodView('competition:calendar',
        #                     competition='sydney-university',
        #                     season='summer-2012')
        self.get('competition:calendar',
                 competition='sydney-university',
                 season='summer-2012')
        self.response_200()

    def test_season_videos(self):
        self.assertGoodView('competition:season-videos',
                            'sydney-university',
                            'summer-2012')

    def test_club(self):
        self.assertGoodView('competition:club',
                            'sydney-university',
                            'summer-2012',
                            'bare-back-riders')

    def test_club_calendar(self):
        self.assertGoodView('competition:calendar',
                            competition='sydney-university',
                            season='summer-2012',
                            club='bare-back-riders')

    def test_division(self):
        # self.assertGoodView('competition:division',
        #                     'sydney-university',
        #                     'summer-2012',
        #                     'mixed-1')
        self.get('competition:division',
                 'sydney-university', 'summer-2012', 'mixed-1')
        self.response_200()

    def test_division_calendar(self):
        # self.assertGoodView('competition:division',
        #                     competition='sydney-university',
        #                     season='summer-2012',
        #                     division='mixed-1')
        self.get('competition:calendar',
                 competition='sydney-university',
                 season='summer-2012',
                 division='mixed-1')
        self.response_200()

    def test_stage(self):
        # self.assertGoodView('competition:stage',
        #                     'sydney-university',
        #                     'summer-2012',
        #                     'mixed-1',
        #                     'round-robin')
        self.get('competition:stage',
                 'sydney-university',
                 'summer-2012',
                 'mixed-1',
                 'round-robin')
        self.response_200()

    def test_match(self):
        self.assertGoodView('competition:match',
                            'sydney-university',
                            'summer-2012',
                            'mixed-1',
                            '1')

    def test_match_gone(self):
        res = self.get('competition:match',
                       'sydney-university',
                       'summer-2012',
                       'mixed-1',
                       '5000')
        self.assertEqual(res.status_code, 410)


    def test_match_video(self):
        self.assertGoodView('competition:match-video',
                            'sydney-university',
                            'summer-2012',
                            'mixed-1',
                            '1')

    def test_match_video_gone(self):
        res = self.get('competition:match-video',
                       'sydney-university',
                       'summer-2012',
                       'mixed-1',
                       '5000')
        self.assertEqual(res.status_code, 410)


class FrontEndTests(TestCase):

    fixtures = [
        'user.json',
        'competition.json',
        'draw_format.json',
        'club.json',
        'person.json',
        'season.json',
        'place.json',
        'division.json',
        'stage.json',
        'team.json',
        'match.json',
    ]

    def test_competition_list(self):
        response = self.client.get('/competition/')
        self.assertContains(response, 'Sydney University', status_code=200)
        self.assertContains(response, 'Edinburgh Touch Superleague')
        # Cardiff is disabled, so should not be available
        self.assertNotContains(response, 'Cardiff Touch Superleague')

    def test_season_list(self):
        response = self.client.get('/competition/sydney-university/')
        self.assertContains(response, 'Summer 2012-2013', status_code=200)
        self.assertContains(response, 'Winter 2013')

    def test_division_list(self):
        response = self.client.get('/competition/sydney-university/summer-2012/')
        self.assertContains(response, 'Mixed 1', status_code=200)
        self.assertContains(response, 'Mixed 2')
        self.assertNotContains(response, 'Mixed 3')

    def test_division_view(self):
        response = self.client.get('/competition/sydney-university/summer-2012/mixed-1/')
        self.assertEquals(response.status_code, 200)
        response = self.client.get('/competition/sydney-university/summer-2012/mixed-2/')
        self.assertEquals(response.status_code, 200)
        response = self.client.get('/competition/sydney-university/summer-2012/mixed-3/')
        self.assertEquals(response.status_code, 404)

    def test_upcoming_matches(self):
        # Fixture loads the date in order, but for this specific test case we
        # want to ensure we have some games in the distant future.
        for m in Match.objects.filter(date=date(2013, 9, 1)):
            m.date = date(2023, 9, 1)
            m.save()

        response = self.client.get('/competition/sydney-university/summer-2012/')
        self.assertContains(response, 'Sept. 1, 2023', status_code=200)

    def test_division_match_list(self):
        link = '<a href="{0}">{1}</a>'

        division = {
            'competition': 'sydney-university',
            'season': 'summer-2012',
            'division': 'mixed-1',
        }

        teams = {
            'Blue': 'blue',
            'Green &amp; Gold': 'green-gold',
            'Red': 'red',
            'Yellow': 'yellow',
            'Black': 'black',
            'Orange': 'orange',
        }

        # GET the page for the division, and ensure it has links to
        # team page for each Team.
        url = reverse_lazy('competition:division', kwargs=division)
        response = self.client.get(url)

        for title, slug in teams.items():
            team = dict(team=slug, **division)
            url = reverse_lazy('competition:team', kwargs=team)
            self.assertContains(response, link.format(url, title))

            # GET the team page and ensure we play each other team
            # by seeing that there are links to all other teams.
            # This is dictated by the `fixture`, not logic that in
            # every division a team must play all other teams.
            response2 = self.client.get(url)
            for title2, slug2 in teams.items():
                team = dict(team=slug2, **division)
                url2 = reverse_lazy('competition:team', kwargs=team)
                link2 = link.format(url2, title2)

                # Team page should not have a link to itself.
                if slug == slug2:
                    self.assertNotContains(response2, link2)
                else:
                    self.assertContains(response2, link2)

    def test_team_calendar(self):
        calendar = {
            'competition': 'sydney-university',
            'season': 'summer-2012',
            'division': 'mixed-1',
            'team': 'blue',
        }
        url = reverse_lazy('competition:calendar', kwargs=calendar)
        res = self.client.get(url)
        self.assertContains(res, 'Blue vs Red')
        self.assertContains(res, 'Blue vs Black')
        self.assertContains(res, 'Blue vs Orange')
        self.assertContains(res, 'Blue vs Yellow')
        self.assertContains(res, 'Blue vs Green & Gold')
