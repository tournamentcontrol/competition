import unittest
from datetime import time

from django.core.urlresolvers import reverse
from django.test.utils import override_settings
from test_plus import TestCase as BaseTestCase

from tournamentcontrol.competition import test_factory as factories
from tournamentcontrol.competition.models import (
    Club,
    Division,
    Match,
    Season,
    StageGroup,
    Team,
    UndecidedTeam,
)


class TestCase(BaseTestCase):
    """
    Add some extra assertions that we can use to simplify tests below.
    """
    def assertGoodEditView(self, viewname, *args, **kwargs):
        test_query_count = kwargs.pop('test_query_count', 50)
        self.assertLoginRequired(viewname, *args)
        with self.login(username='gary@touch.asn.au'):
            self.assertGoodView(
                viewname, test_query_count=test_query_count, *args)
            if kwargs:
                self.post(viewname, *args, **kwargs)
                self.response_302()

    def assertGoodDeleteView(self, viewname, *args):
        self.assertLoginRequired(viewname, *args)
        with self.login(username='gary@touch.asn.au'):
            self.get(viewname, *args)
            self.response_405()
            self.post(viewname, *args)
            self.response_302()

    def assertGoodNamespace(self, namespace, *args, **kwargs):
        self.assertGoodEditView('%s:add' % namespace, *args[:-1], **kwargs)
        self.assertGoodEditView('%s:edit' % namespace, *args, **kwargs)
        self.assertGoodDeleteView('%s:delete' % namespace, *args)


class GoodViewTests(TestCase):

    fixtures = [
        'user.json',
        'competition.json',
        'draw_format.json',
        'club.json',
        'person.json',
        'season.json',
        'place.json',
        'division.json',
        'stage.json',
        'team.json',
        'match.json',
        'clubassociation.json',
        'teamrole.json',
        'teamassociation.json',
    ]

    def test_reorder_down(self):
        url = self.reverse('admin:fixja:reorder',
                           model='stagegroup', pk=2, direction='down')
        with self.login(username='gary@touch.asn.au'):
            # Without a HTTP_REFERER set this should throw a 404
            res = self.client.get(url)
            self.response_404(res)

            # The first time we try to shift this down it will work because it
            # isn't last in it's set.
            res = self.client.get(url, {}, HTTP_REFERER='http://testserver/')
            self.response_302(res)

            # This time, however, it should fail because we're now last.
            res = self.client.get(url, {}, HTTP_REFERER='http://testserver/')
            self.response_404(res)

    def test_reorder_up(self):
        url = self.reverse('admin:fixja:reorder',
                           model='stagegroup', pk=1, direction='up')
        with self.login(username='gary@touch.asn.au'):
            # Without a HTTP_REFERER set this should throw a 404
            res = self.client.get(url)
            self.response_404(res)

            # The first time we try to shift this up it will work because it
            # isn't first in it's set.
            res = self.client.get(url, {}, HTTP_REFERER='http://testserver/')
            self.response_302(res)

            # This time, however, it should fail because we're now first.
            res = self.client.get(url, {}, HTTP_REFERER='http://testserver/')
            self.response_404(res)

    def test_reorder_left(self):
        url = self.reverse('admin:fixja:reorder',
                           model='stagegroup', pk=1, direction='left')
        with self.login(username='gary@touch.asn.au'):
            # A direction of "left" is invalid and should throw a 404
            res = self.client.get(url, {}, HTTP_REFERER='http://testserver/')
            self.response_404(res)

    def test_index(self):
        self.assertLoginRequired('admin:fixja:index')
        with self.login(username='gary@touch.asn.au'):
            self.get('admin:fixja:index')
            self.response_302()

    def test_competition(self):
        self.assertLoginRequired('admin:fixja:competition:list')
        with self.login(username='gary@touch.asn.au'):
            self.assertGoodView('admin:fixja:competition:list')
        self.assertGoodNamespace(
            'admin:fixja:competition', 1)

    def test_clubrole(self):
        self.assertGoodNamespace(
            'admin:fixja:competition:clubrole', 1, 1,
            data={'name': 'Club Role'})

    def test_teamrole(self):
        self.assertGoodNamespace(
            'admin:fixja:competition:teamrole', 1, 1,
            data={'name': 'Team Role'})

    def test_season(self):
        self.assertGoodNamespace(
            'admin:fixja:competition:season', 1, 1)

    def test_season_exclusion(self):
        self.assertGoodNamespace(
            'admin:fixja:competition:season:seasonexclusiondate', 1, 2, 1)

    def test_timeslot(self):
        self.assertGoodNamespace(
            'admin:fixja:competition:season:seasonmatchtime', 1, 2, 1)

    def test_venue(self):
        self.assertGoodNamespace(
            'admin:fixja:competition:season:venue', 1, 1, 1)

    def test_ground(self):
        self.assertGoodNamespace(
            'admin:fixja:competition:season:venue:ground', 1, 1, 1, 2)

    def test_division(self):
        self.assertGoodNamespace(
            'admin:fixja:competition:season:division', 1, 1, 1)

    def test_division_exclusion(self):
        self.assertGoodNamespace(
            'admin:fixja:competition:season:division:divisionexclusiondate', 1, 1, 3, 1)

    def test_team(self):
        team = factories.TeamFactory.create()
        self.assertGoodNamespace(
            'admin:fixja:competition:season:division:team',
            team.division.season.competition.pk,
            team.division.season.pk,
            team.division.pk,
            team.pk)

    def test_teamassociation(self):
        team_association = factories.TeamAssociationFactory.create()
        self.assertGoodNamespace(
            'admin:fixja:competition:season:division:team:teamassociation',
            team_association.team.division.season.competition.pk,
            team_association.team.division.season.pk,
            team_association.team.division.pk,
            team_association.team.pk,
            team_association.pk)

    def test_stage(self):
        self.assertGoodNamespace(
            'admin:fixja:competition:season:division:stage', 1, 1, 1, 1, test_query_count=100)

    def test_match(self):
        self.assertGoodNamespace(
            'admin:fixja:competition:season:division:stage:match', 1, 1, 1, 1, 1)

    def test_matchvideo(self):
        self.assertGoodNamespace(
            'admin:fixja:competition:season:division:stage:match:matchvideo', 1, 1, 1, 1, 1, 1)

    def test_pool(self):
        self.assertGoodNamespace(
            'admin:fixja:competition:season:division:stage:stagegroup', 1, 1, 2, 4, 1)

    def test_club(self):
        self.assertLoginRequired('admin:fixja:club:list')
        with self.login(username='gary@touch.asn.au'):
            self.assertGoodView('admin:fixja:club:list')
        self.assertGoodNamespace(
            'admin:fixja:club', 1)

    def test_person(self):
        person = factories.PersonFactory.create()
        self.assertFalse(person.statistics.count())
        self.assertGoodNamespace(
            'admin:fixja:club:person',
            person.club.pk,
            person.pk)

    def test_format(self):
        self.assertLoginRequired('admin:fixja:format:list')
        with self.login(username='gary@touch.asn.au'):
            self.assertGoodView('admin:fixja:format:list')
        self.assertGoodNamespace('admin:fixja:format', 1)

    @unittest.skip("Not refactored as a namespace yet")
    def test_list_drawformat(self):
        self.assertLoginRequired('admin:fixja:report:list')
        with self.login(username='gary@touch.asn.au'):
            self.assertGoodView('admin:fixja:report:list')

    # code coverage

    def test_season_report(self):
        self.assertLoginRequired('admin:fixja:season-report', 1, 1)
        with self.login(username='gary@touch.asn.au'):
            self.assertGoodView('admin:fixja:season-report', 1, 1)

    def test_season_summary(self):
        self.assertLoginRequired('admin:fixja:season-summary', 1, 1)
        with self.login(username='gary@touch.asn.au'):
            self.assertGoodView('admin:fixja:season-summary', 1, 1)

    def test_progress_teams(self):
        self.assertLoginRequired(
            'admin:fixja:competition:season:division:stage:draw:progress',
            1, 1, 1, 1)
        with self.login(username='gary@touch.asn.au'):
            self.get(
                'admin:fixja:competition:season:division:stage:draw:progress',
                1, 1, 1, 1)
            self.response_410()

    def test_edit_person(self):
        self.assertLoginRequired('admin:fixja:club:person:add', 1)
        self.assertLoginRequired('admin:fixja:club:person:edit', 1, 1)
        with self.login(username='gary@touch.asn.au'):
            self.assertGoodView('admin:fixja:club:person:add', 1)
            self.assertGoodView('admin:fixja:club:person:edit', 1, 1)

    def test_edit_clubassociation(self):
        self.assertLoginRequired('admin:fixja:club:clubassociation:add', 1)
        self.assertLoginRequired('admin:fixja:club:clubassociation:edit', 1, 1)
        with self.login(username='gary@touch.asn.au'):
            self.assertGoodView('admin:fixja:club:clubassociation:add', 1)
            self.assertGoodView('admin:fixja:club:clubassociation:edit', 1, 1)

    # def test_delete_competition(self):
    #     self.assertLoginRequired('admin:fixja:competition:delete', 1)
    #     with self.login(username='gary@touch.asn.au'):
    #         self.get('admin:fixja:competition:delete', 1)
    #         self.response_405()
    #         self.post('admin:fixja:competition:delete', 1)
    #         self.response_302()

    def test_perms_views(self):
        self.assertLoginRequired(
            'admin:fixja:competition:perms', 1)
        self.assertLoginRequired(
            'admin:fixja:competition:season:perms', 1, 1)
        # self.assertLoginRequired(
        #     'admin:fixja:competition:season:division:perms', 1, 1, 1)
        self.assertLoginRequired(
            'admin:fixja:competition:season:division:team:perms', 1, 1, 1, 1)

        with self.login(username='gary@touch.asn.au'):
            self.assertGoodView(
                'admin:fixja:competition:perms', 1)
            self.assertGoodView(
                'admin:fixja:competition:season:perms', 1, 1)
            # self.assertGoodView(
            #     'admin:fixja:competition:season:division:perms', 1, 1, 1)
            with self.assertNumQueriesLessThan(100):
                self.get(
                    'admin:fixja:competition:season:division:team:perms',
                    1, 1, 1, 1)
            self.response_200()

    def test_registration_form(self):
        self.assertLoginRequired(
            'admin:fixja:club:registration-form', 1, 1, 'html')
        with self.login(username='gary@touch.asn.au'):
            self.assertGoodView(
                'admin:fixja:club:registration-form', 1, 1, 'html')

    @unittest.expectedFailure
    def test_edit_team_members(self):
        self.assertLoginRequired('admin:fixja:club:team:edit', 1, 1, 1)
        with self.login(username='gary@touch.asn.au'):
            self.assertGoodView('admin:fixja:club:team:edit', 1, 1, 1)


class BackendTests(BaseTestCase):

    fixtures = [
        'user.json',
        'competition.json',
        'draw_format.json',
        'club.json',
        'person.json',
        'season.json',
        'place.json',
        'division.json',
        'stage.json',
        'team.json',
        'match.json',
    ]

    def setUp(self):
        self.client.login(username='gary@touch.asn.au', password='password')

    def test_task_0058_highest_point_scorer(self):
        """
        Actual test case logic, invoked by simple wrappers which switch the
        database backend out.

        https://bitbucket.org/tournamentcontrol/competition/issue/58/make-sure-any-custom-raw-sql-will-work
        """
        self.assertGoodView(
            'admin:fixja:competition:season:division:scorers', 1, 1, 1)

    def test_add_division(self):
        """
        Using the admin interface, add a new division and check that it
        appears in the front-end. Later update the form data and repost
        to ensure the front-end is also updated.
        """
        season = Season.objects.earliest('pk')
        data = {
            'title': 'Mixed 4',
            'short_title': 'X4',
            'points_formula_0': '3',
            'points_formula_1': '2',
            'points_formula_2': '1',
            'points_formula_3': '',
            'points_formula_4': '',
            'points_formula_5': '',
            'forfeit_for_score': '5',
            'forfeit_against_score': '0',
            'include_forfeits_in_played': '1',
            'slug': '',
            'slug_locked': '0',
        }
        post_url = reverse(
            'admin:fixja:competition:season:division:add',
            args=(season.competition_id, season.pk))
        redirect_url = reverse(
            'admin:fixja:competition:season:edit',
            args=(season.competition_id, season.pk))
        response = self.client.post(post_url, data)
        self.assertRedirects(response, redirect_url)

        response = self.client.get('/competition/sydney-university/summer-2012/')
        self.assertContains(response, 'Mixed 4')

        response = self.client.get('/competition/sydney-university/summer-2012/mixed-4/')
        self.assertContains(response, 'Mixed 4')

        data.update({
            'title': '4th Division Mixed',
        })
        division = season.divisions.latest('pk')
        post_url = reverse(
            'admin:competition:competition:season:division:edit',
            args=(season.competition_id, season.pk, division.pk))
        response = self.client.post(post_url, data)
        self.assertRedirects(response, redirect_url)

        response = self.client.get('/competition/sydney-university/summer-2012/')
        self.assertNotContains(response, 'Mixed 4')
        self.assertContains(response, '4th Division Mixed')

    def test_update_club(self):
        """
        Using the admin interface, update an existing Club and ensure that the
        revised attribute is correctly updated.
        """
        club = Club.objects.earliest('pk')
        data = {
            'title': 'Bare Back Riders',
            'short_title': '',
            'abbreviation': 'BBR',
            'website': '',
            'twitter': '',
            'primary': '',
            'slug': 'bare-back-riders',
            'slug_locked': '0',
        }
        self.post('admin:fixja:club:edit', club.pk, data=data)
        self.response_302()

        self.get('admin:fixja:club:list')
        self.assertContains(self.last_response, 'Bare Back Riders')
        self.assertContains(self.last_response, 'BBR')

    def test_match_detail(self):
        # Add scoreline to match or this will be a 404
        Match.objects.filter(pk=1).update(home_team_score=3,
                                          away_team_score=0)
        m = Match.objects.get(pk=1)

        kwargs = {
            'match_id': m.pk,
            'stage_id': m.stage_id,
            'division_id': m.stage.division_id,
            'season_id': m.stage.division.season_id,
            'competition_id': m.stage.division.season.competition_id,
        }
        url = reverse(
            'admin:competition:competition:season:division:stage:match:detail',
            kwargs=kwargs)

        res = self.client.get(url)

        # After GET ensure away_team has no players
        text = '''
        <td colspan="5" class="no_results">
            There are no registered players in this team.
        </td>
        '''
        self.assertContains(res, text, html=True)

        data = {
            'home-0-number': '3',
            'home-0-id': '',
            'home-0-played': '1',
            'home-0-points': '0',
            'home-0-mvp': '0',

            'home-1-number': '8',
            'home-1-id': '',
            'home-1-played': '1',
            'home-1-points': '0',
            'home-1-mvp': '0',

            'home-2-number': '10',
            'home-2-id': '',
            'home-2-played': '1',
            'home-2-points': '0',
            'home-2-mvp': '0',

            'home-3-number': '12',
            'home-3-id': '',
            'home-3-played': '1',
            'home-3-points': '0',
            'home-3-mvp': '0',

            'home-TOTAL_FORMS': '4',
            'home-INITIAL_FORMS': '4',
            'home-MAX_NUM_FORMS': '1000',

            'away-TOTAL_FORMS': '0',
            'away-INITIAL_FORMS': '0',
            'away-MAX_NUM_FORMS': '1000',
        }

        res = self.client.post(url, data=data)
        text = '''
        <ul class="list-group">
            <li class="list-group-item list-group-item-danger">
                Total number of points (0) does not equal total
                number of scores (3) for this team.
            </li>
        </ul>
        '''
        self.assertContains(res, text, html=True)

        data.update({
            'home-0-points': '0',
            'home-1-points': '1',
            'home-2-points': '0',
            'home-3-points': '2',
        })
        res = self.client.post(url, data=data)
        self.assertRedirects(res, reverse('admin:index'))

    @override_settings(USE_TZ=False)
    def test_bug_0068(self):
        """
        While there are no LadderEntry or LadderSummary records for matches
        revealed in the ``match-results`` view, the submission worked file.

        However on subsequent processing the method decorator is invoked
        differently, and there is now "raw" keyword argument - this resulted
        in issue #68.
        """
        data = {
            # first formset
            "matches-INITIAL_FORMS": "3",
            "matches-MAX_NUM_FORMS": "1000",
            "matches-TOTAL_FORMS": "3",

            "matches-0-id": "1",
            "matches-0-home_team_score": "4",
            "matches-0-away_team_score": "3",
            "matches-0-is_forfeit": "0",

            "matches-1-id": "2",
            "matches-1-home_team_score": "",
            "matches-1-away_team_score": "",
            "matches-1-is_forfeit": "0",

            "matches-2-id": 3,
            "matches-2-home_team_score": "",
            "matches-2-away_team_score": "",
            "matches-2-is_forfeit": "0",

            # second formset
            "byes-INITIAL_FORMS": "0",
            "byes-MAX_NUM_FORMS": "1000",
            "byes-TOTAL_FORMS": "0",
        }

        self.post('admin:fixja:match-results',
                  competition_id=1,
                  season_id=1,
                  datestr='20130901',
                  data=data)
        self.response_302()

        data.update({
            "matches-1-home_team_score": "5",
            "matches-1-away_team_score": "2",
        })

        self.post('admin:fixja:match-results',
                  competition_id=1,
                  season_id=1,
                  datestr='20130901',
                  data=data)
        self.response_302()

    def test_enhancement_0024_team(self):
        """
        When editing a Team or UndecidedTeam it should be possible to delete
        the object if there are no matches that team has been assigned to.
        """
        delete_url = self.reverse(
            'admin:fixja:competition:season:division:team:delete',
            competition_id=1,
            season_id=1,
            division_id=1,
            team_id=1)

        # Ensure the edit view response does not include a delete button.
        res = self.get('admin:fixja:competition:season:division:team:edit',
                       competition_id=1,
                       season_id=1,
                       division_id=1,
                       team_id=1)
        self.assertNotContains(res, delete_url)

        # Ensure that visiting the delete view does not respond when matches
        # are associated with the Team.
        self.get(delete_url)
        self.response_410()

        # Ensure that POST to the delete view fails the same as for GET.
        self.post(delete_url)
        self.response_410()

        # Create a new Team and re-check the scenarios.
        team = Team.objects.create(
            division_id=1, title='Brown', slug='brown', order=7)

        # Repeat for new Team.
        delete_url = self.reverse(
            'admin:fixja:competition:season:division:team:delete',
            competition_id=1,
            season_id=1,
            division_id=1,
            team_id=team.pk)

        # Ensure the edit view response does not include a delete button.
        self.get('admin:fixja:competition:season:division:team:edit',
                 competition_id=1,
                 season_id=1,
                 division_id=1,
                 team_id=team.pk)
        self.assertNotContains(self.last_response, delete_url)

        # Ensure that visiting the delete view is not allowed when there are no
        # matches but you use a GET request.
        self.get(delete_url)
        self.response_405()

        # Ensure that POST to the delete view redirects.
        self.post(delete_url)
        self.response_302()

        # Subsequent GET request to the edit view should be a 404.
        self.get('admin:fixja:competition:season:division:team:edit',
                 competition_id=1,
                 season_id=1,
                 division_id=1,
                 team_id=team.pk)
        self.response_404()

    def test_enhancement_0024_undecidedteam(self):
        """
        When editing a Team or UndecidedTeam it should be possible to delete
        the object if there are no matches that team has been assigned to.
        """
        delete_url = self.reverse(
            'admin:fixja:competition:season:division:stage:undecidedteam:delete',
            competition_id=1,
            season_id=1,
            division_id=1,
            stage_id=2,
            team_id=1)

        # Ensure the edit view response does not include a delete button.
        self.get('admin:fixja:competition:season:division:stage:undecidedteam:edit',
                 competition_id=1,
                 season_id=1,
                 division_id=1,
                 stage_id=2,
                 team_id=1)
        self.assertNotContains(self.last_response, delete_url)

        # Ensure that visiting the delete view does not respond when matches
        # are associated with the Team.
        self.get(delete_url)
        self.response_410()

        # Ensure that POST to the delete view fails the same as for GET.
        self.post(delete_url)
        self.response_410()

        # Create a new Team and re-check the scenarios.
        team = UndecidedTeam.objects.create(stage_id=2, formula="P7")

        # Repeat for new Team.
        delete_url = self.reverse(
            'admin:fixja:competition:season:division:stage:undecidedteam:delete',
            competition_id=1,
            season_id=1,
            division_id=1,
            stage_id=2,
            team_id=team.pk)

        # Ensure the edit view response does not include a delete button.
        self.get('admin:fixja:competition:season:division:stage:undecidedteam:edit',
                 competition_id=1,
                 season_id=1,
                 division_id=1,
                 stage_id=2,
                 team_id=team.pk)
        self.assertNotContains(self.last_response, delete_url)

        # Ensure that visiting the delete view is not allowed when there are no
        # matches but you use a GET request.
        self.get(delete_url)
        self.response_405()

        # Ensure that POST to the delete view redirects.
        self.post(delete_url)
        self.response_302()

        # Subsequent GET request to the edit view should be a 404.
        self.get('admin:fixja:competition:season:division:stage:undecidedteam:edit',
                 competition_id=1,
                 season_id=1,
                 division_id=1,
                 stage_id=2,
                 team_id=team.pk)
        self.response_404()

    def test_bug_80_add_stagegroup(self):
        "Add Pool should gain order equal to max(order) + 1"

        kwargs = {
            'competition_id': 1,
            'season_id': 1,
            'division_id': 2,
            'stage_id': 4,
        }

        url = reverse(
            'admin:fixja:competition:season:division:stage:stagegroup:add',
            kwargs=kwargs)

        data = {
            'title': 'Group Z',
            'short_title': '',
            'slug': '',
            'slug_locked': '0',
            'carry_ladder': '0',
        }
        res = self.client.post(url, data=data)

        pool = StageGroup.objects.filter(stage_id=4).latest('order')
        kwargs['pool_id'] = pool.pk
        redirect_to = reverse(
            'admin:fixja:competition:season:division:stage:stagegroup:edit',
            kwargs=kwargs)
        self.assertRedirects(res, redirect_to)

        orders = StageGroup.objects.filter(
            stage_id=4).values_list('order', flat=True)

        self.assertEqual(list(orders), [1, 2, 3])

    def test_bug_0115_add_duplicate_team_name(self):
        """
        When adding two teams to the same division, it should not allow the
        second one because of database integrity constraints.
        """
        division = Division.objects.earliest('pk')
        post_url = reverse(
            'admin:competition:competition:season:division:team:add',
            args=(division.season.competition_id, division.season_id,
                  division.pk))

        data = {
            'title': division.teams.earliest('order').title,
        }
        response = self.client.post(post_url, data)
        self.assertContains(
            response, "Team name must be unique in this division.")

    def test_team_add(self):
        data = {
            'title': 'New Team',  # current value
            'short_title': '',
            'names_locked': '0',
            'timeslots_after_0': '',
            'timeslots_after_1': '',
            'timeslots_before_0': '',
            'timeslots_before_1': '',
            'team_clashes': [],
        }
        self.post('admin:fixja:competition:season:division:team:add',
                  competition_id=1,
                  season_id=1,
                  division_id=1,
                  data=data)
        self.response_302()
        # Ensure that the team was created and that it has a slug
        team = Team.objects.get(title='New Team')
        self.assertEqual(team.slug, 'new-team')

    def test_team_edit(self):
        data = {
            'title': 'Blue',  # current value
            'short_title': '',
            'names_locked': '0',
            'timeslots_after_0': '19',  # new value
            'timeslots_after_1': '20',  # new value
            'timeslots_before_0': '',
            'timeslots_before_1': '',
            'team_clashes': [],
        }

        self.post('admin:fixja:competition:season:division:team:edit',
                  competition_id=1,
                  season_id=1,
                  division_id=1,
                  team_id=1,
                  data=data)
        self.response_302()

        # Ensure that the team was updated
        team = Team.objects.get(title='Blue')
        self.assertEqual(team.slug, 'blue')
        self.assertEqual(team.timeslots_after, time(19, 20))

        # Try again, update the title and make sure the slug changes
        data['title'] = 'Magenta'
        self.post('admin:fixja:competition:season:division:team:edit',
                  competition_id=1,
                  season_id=1,
                  division_id=1,
                  team_id=1,
                  data=data)
        self.response_302()
        team = Team.objects.get(title='Magenta')
        self.assertEqual(team.slug, 'magenta')
