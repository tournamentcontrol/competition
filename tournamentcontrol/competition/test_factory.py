import factory
import factory.fuzzy
from django.contrib.auth import get_user_model

from tournamentcontrol.competition import models


class SitemapNodeBaseFactory(factory.DjangoModelFactory):
    """
    For any model which inherits from
    ``touchtechnology.common.models.SitemapNodeBase`` we need to execute the
    ``clean`` method which will populate the ``slug`` attribute according to
    the business logic of that method.
    """
    @factory.post_generation
    def post(obj, create, extracted, **kwargs):
        obj.clean()


class OrderedSitemapNodeFactory(SitemapNodeBaseFactory):
    """
    When adding a series of ``OrderedSitemapNode`` objects we should ensure
    that they are appropriately "ordered".
    """
    order = factory.Sequence(lambda n: (n + 1))


class RankDivisionFactory(OrderedSitemapNodeFactory):
    class Meta:
        model = models.RankDivision

    title = factory.Sequence(lambda n: u"Division %d" % n)


class ClubFactory(SitemapNodeBaseFactory):
    class Meta:
        model = models.Club

    title = factory.Faker('company')


class CompetitionFactory(OrderedSitemapNodeFactory):
    class Meta:
        model = models.Competition

    title = factory.Sequence(lambda n: u"Competition %d" % n)


class SeasonFactory(OrderedSitemapNodeFactory):
    class Meta:
        model = models.Season

    title = factory.Sequence(
        lambda n: unicode(range(2015, 1900, -1)[n]))
    timezone = factory.Faker('timezone')

    competition = factory.SubFactory(CompetitionFactory)


class VenueFactory(OrderedSitemapNodeFactory):
    class Meta:
        model = models.Venue

    title = factory.Sequence(lambda n: u"Venue %d" % (n + 1))
    timezone = factory.SelfAttribute('season.timezone')

    season = factory.SubFactory(SeasonFactory)


class GroundFactory(OrderedSitemapNodeFactory):
    class Meta:
        model = models.Ground

    title = factory.Sequence(lambda n: u"Ground %d" % (n + 1))
    timezone = factory.SelfAttribute('venue.timezone')

    venue = factory.SubFactory(VenueFactory)


class DivisionFactory(OrderedSitemapNodeFactory):
    class Meta:
        model = models.Division

    title = factory.Sequence(lambda n: u"Division %d" % (n + 1))
    points_formula = u"3*win + 2*draw + 1*loss"
    forfeit_for_score = 5
    forfeit_against_score = 0
    include_forfeits_in_played = True
    games_per_day = 2

    season = factory.SubFactory(SeasonFactory)


class TeamFactory(OrderedSitemapNodeFactory):
    class Meta:
        model = models.Team

    title = factory.Sequence(lambda n: u"Team %d" % (n + 1))

    club = factory.SubFactory(ClubFactory)
    division = factory.SubFactory(DivisionFactory)


class StageFactory(OrderedSitemapNodeFactory):
    class Meta:
        model = models.Stage

    title = factory.Sequence(lambda n: u"Stage %d" % (n + 1))

    division = factory.SubFactory(DivisionFactory)


class StageGroupFactory(OrderedSitemapNodeFactory):
    class Meta:
        model = models.StageGroup

    title = factory.Sequence(lambda n: u"Pool %d" % (n + 1))

    stage = factory.SubFactory(StageFactory)


class MatchFactory(factory.DjangoModelFactory):
    class Meta:
        model = models.Match

    stage = factory.SubFactory(StageFactory)

    home_team = factory.SubFactory(
        TeamFactory, division=factory.SelfAttribute('..stage.division'))
    away_team = factory.SubFactory(
        TeamFactory, division=factory.SelfAttribute('..stage.division'))


class UserFactory(factory.DjangoModelFactory):
    class Meta:
        model = get_user_model()

    first_name = factory.Faker('first_name')
    last_name = factory.Faker('last_name')

    username = factory.Sequence(lambda n: u'user%04d' % (n + 1))
    email = factory.Sequence(lambda n: u"user%03d@example.com" % (n + 1))

    password = 'crypt$$azoV33FJ2h3AA'  # pre-crypted "password"
    is_active = True


class PersonFactory(factory.DjangoModelFactory):
    class Meta:
        model = models.Person

    first_name = factory.SelfAttribute('user.first_name')
    last_name = factory.SelfAttribute('user.last_name')

    date_of_birth = factory.Faker(
        'date_time_between', start_date="-30y", end_date="-20y")
    gender = factory.fuzzy.FuzzyChoice(['M', 'F', 'X'])

    club = factory.SubFactory(ClubFactory)
    user = factory.SubFactory(UserFactory)


class TeamAssociationFactory(factory.DjangoModelFactory):
    class Meta:
        model = models.TeamAssociation

    team = factory.SubFactory(TeamFactory)
    person = factory.SubFactory(PersonFactory)
