#!/bin/bash

export PYTHONPATH=$PYTHONPATH:$(pwd)

pushd tournamentcontrol/competition
django-admin makemessages -l en-AU -l de -l fr -l ja
django-admin compilemessages
popd
