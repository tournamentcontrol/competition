import glob
import os
from setuptools import setup, find_packages

version = '2.4.14'
author = 'Touch Technology Pty Ltd'
author_email = 'support@touchtechnology.com.au'
url = 'https://bitbucket.org/tournamentcontrol/competition/'

setup(
    name='tournamentcontrol-competition',
    version=version,
    author=author,
    author_email=author_email,
    url=url,
    description='Competition management system',
    packages=find_packages(exclude=["test_app"]),
    install_requires=[
        'touchtechnology-admin>=3.2.11,<3.3',
        'touchtechnology-content>=3.0.2',
        'touchtechnology-oembed>=0.1.3,<0.2',
        'django-celery>=3.1.0',
        'django-formtools>=1.0',
        'django-mathfilters',
        'icalendar>=3.9.0,<4',
        'pygraphviz>=1.2,<1.3',
        'pyparsing>=2.0.3,<3',
        'beautifulsoup4>=4.4,<5',
        'requests>=2.7,<3',
        'numpy',
    ],
    include_package_data=True,
    namespace_packages=['tournamentcontrol'],
    zip_safe=False,
)
