from django.conf import settings
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()
from django.contrib.admin import autodiscover
from touchtechnology.admin import sites as admin
autodiscover()

from tournamentcontrol.competition.sites import (
    calculator,
    competition,
    registration,
)

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'test_app.views.home', name='home'),
    # url(r'^test_app/', include('test_app.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    url(r'^calculator/', include(calculator.urls)),
    url(r'^competition/', include(competition.urls)),
    url(r'^registration/', include(registration.urls),
        kwargs={'admin': admin.site}),
    url(r'^', include('touchtechnology.common.urls')),
)

urlpatterns += static(settings.MEDIA_URL,
                      document_root=settings.MEDIA_ROOT)
urlpatterns += staticfiles_urlpatterns()
