from os import environ
from os.path import dirname, join as basejoin, realpath
from django.core.urlresolvers import reverse_lazy

join = lambda *args: realpath(basejoin(*args))

PROJECT_DIR = join(dirname(__file__), '..')

DEBUG = True
TEMPLATE_DEBUG = DEBUG

database = environ.get('DATABASE')

if database == 'postgresql':
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql_psycopg2',
            'HOST': '127.0.0.1',
            'PORT': 5432,
            'USER': environ.get('PG_USER'),
            'PASSWORD': environ.get('PG_PASSWORD'),
            'NAME': 'test',
        },
    }
elif database == 'mysql':
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.mysql',
            'HOST': '127.0.0.1',
            'PORT': 3306,
            'USER': environ.get('MYSQL_USER'),
            'PASSWORD': environ.get('MYSQL_PASSWORD'),
            'NAME': 'test',
            'OPTIONS': {
                'init_command': 'SET character_set_connection = utf8, '
                                'collation_connection = utf8_unicode_ci',
            },
        },
    }
else:
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
        },
    }

PASSWORD_HASHERS = (
    'django.contrib.auth.hashers.CryptPasswordHasher',
)

TIME_ZONE = 'Australia/Sydney'
LANGUAGE_CODE = 'en-AU'

USE_TZ = True

STATIC_URL = '/static/'
STATIC_ROOT = join(PROJECT_DIR, 'static')

MEDIA_URL = '/media/'
MEDIA_ROOT = join(PROJECT_DIR, 'media')

SECRET_KEY = '2bksb4rhbv7i1$!5xzux0&amp;&amp;sl2@de@k6sxb4(zlj4l)+xds#2i'
SITE_ID = 1

ROOT_URLCONF = 'test_app.urls'
WSGI_APPLICATION = 'test_app.wsgi.application'

INSTALLED_APPS = (
    'mptt',
    'guardian',
    'bootstrap3',
    'django_gravatar',
    'mathfilters',
    'oembed',

    'touchtechnology.common',
    'touchtechnology.admin',
    'touchtechnology.content',
    'tournamentcontrol.competition',

    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.humanize',
    'django.contrib.messages',
    'django.contrib.sessions',
    'django.contrib.sites',
)

AUTHENTICATION_BACKENDS = (
    'touchtechnology.common.backends.auth.UserSubclassBackend',
    'touchtechnology.common.backends.auth.EmailUserSubclassBackend',
    'guardian.backends.ObjectPermissionBackend',
)

LOGIN_URL = reverse_lazy('accounts:login')

ANONYMOUS_USER_ID = -1
ANONYMOUS_DEFAULT_USERNAME_VALUE = 'anonymous'

FIXTURE_DIRS = (
    join(PROJECT_DIR, 'fixtures'),
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.request',
    'django.core.context_processors.static',
    'django.contrib.messages.context_processors.messages',
    'touchtechnology.common.context_processors.env',
    'touchtechnology.common.context_processors.query_string',
    'touchtechnology.common.context_processors.tz',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sites.middleware.CurrentSiteMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'touchtechnology.common.middleware.TimezoneMiddleware',
    'touchtechnology.content.middleware.SitemapNodeMiddleware',
)

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        },
    },
    'formatters': {
        'verbose': {
            'format': '%(levelname)-10s %(name)-30s %(asctime)-27s '
                      '%(message)s',
        },
        'simple': {
            'format': '%(levelname)s %(message)s',
        },
    },
    'handlers': {
        'null': {
            'class': 'django.utils.log.NullHandler',
        },
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'verbose',
        },
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        },
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
        'touchtechnology': {
            'handlers': ['null'],
            'level': 'ERROR',
        },
        'tournamentcontrol': {
            'handlers': ['null'],
            'level': 'ERROR',
        },
    },
}
